(function() {
  'use strict';
  // shhh
  var alternatives = ["iV2ViNJFZC8", "dY8WEMm_ZzI", "jlSF0dtDRD8",
    "qRuNxHqwazs"];
  document.querySelector('#boxmein').ondblclick = function() {
    window.location = "https://www.youtube.com/watch?v=" +
                      alternatives[Math.floor(Math.random()*alternatives.length)];
  };

  // date titles
  for (var i=0, is = document.querySelectorAll('.date'); i < is.length; i++) {
    var innerNum = Number(is[i].innerHTML);
    if (!isNaN(innerNum)) {
      is[i].title = new Date(innerNum).toString();
    }
  }

  function clearHighlights() {
    for (var i=0, is=document.querySelectorAll('.highlighted'); i<is.length;i++) {
      is[i].className = is[i].className.replace("highlighted", '');
    }
  }

  // map highlights legend
  function highlightLegend(id) {
    // remove old highlights
    clearHighlights();

    var d = document.getElementById("l-"+id);
    if (d) {
      d.className += ' highlighted';
    }
  }

  // process numbers on the map
  for (var i=0, is=document.querySelectorAll('.subway .num'); i<is.length; i++) {
    is[i].onclick = function(evt) {
      highlightLegend(this.innerHTML);
      this.className += ' highlighted';
    };
    // add a n-# id for highlighting
    is[i].id = 'n-' + is[i].innerHTML;
  }

  // legend highlights map
  function highlightMap(id) {
    clearHighlights();

    var d = document.getElementById("n-"+id);
    if (d) {
      d.className += ' highlighted';
    }
  }

  for (var i=0, is=document.querySelectorAll('.legend-item:not(.no-click)');i<is.length;i++) {
    is[i].onclick = function(evt) {
      highlightMap(this.id.slice(2));
      this.className += ' highlighted';
    }
  }

})();
