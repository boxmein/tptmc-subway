TPTMC Subway Map Changelog
Last updated: 24 July 2015

24 July 2015: Release 9
  * Adjusted more rails and tunnels to account for the modeling changes
  * Removed #14 and shifted everything in place

24 July 2015: Release 8
  * New features:
     - SirGuacamole's / JandCand0's / Dud3's nether portal
     - jward212's jungle base
     - boltza323's base
  * Adjusted some rails to account for a rebuild
  * TODO: Roof rail

22 June 2015: Release 7
  * New features: 
     - jward212's stronghold and Hunger Games Arena
     - Finalflash50's Base
     - Simon's Base
  * Added coordinates to many existing features

20 June 2015: Release 6
  * Fixed new features
  * Adjusted map sizes a bit, maybe they're more in scale (hi @jacob1)
  * Added new Mesa rail

20 June 2015: Release 5
  * New features:
    - Wither Skeleton farm
    - Guardian farm
    - Sg_Voltage's base
    - SanderTheDragon's base
    - jward212's base

27 May 2015: Release 4
  * Added analytics

27 May 2015: Release 3
  * Split the mniip/Ruben base node

27 May 2015: Release 2
  * Firefox doesn't have innerText --> everything uses innerHTML now
  * Added viewport tag --> magic responsive design

27 May 2015: Release 1
  * Initial design
  * Initial data
  * Initial script
  * Added interactivity
